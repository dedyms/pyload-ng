FROM registry.gitlab.com/dedyms/debian:latest as tukang
LABEL build-date=$BUILDDATE
RUN apt update && apt install -y --no-install-recommends python3-pip python3-dev libcurl4-gnutls-dev libgnutls28-dev git build-essential && pip3 install --upgrade pip setuptools wheel distlib && apt clean && rm -rf /var/lib/apt/lists/* && apt clean


# Build in user
USER $CONTAINERUSER
WORKDIR /home/$CONTAINERUSER/
RUN mkdir -p /home/$CONTAINERUSER/pyload/downloads/ && mkdir -p /home/$CONTAINERUSER/pyload/config/settings/
COPY --chown=$CONTAINERUSER:$CONTAINERUSER pyload.cfg /home/$CONTAINERUSER/pyload/config/settings/pyload.cfg
RUN git clone -b develop --depth=1 https://github.com/pyload/pyload pyload-git
RUN pip3 install --user --no-cache-dir /home/$CONTAINERUSER/pyload-git && rm -rf /home/$CONTAINERUSER/.cache/pip && rm -rf /home/$CONTAINERUSER/pyload-git


# Extract Home Debian to New Container
FROM registry.gitlab.com/dedyms/debian:latest
COPY --from=tukang --chown=$CONTAINERUSER:$CONTAINERUSER /home/$CONTAINERUSER /home/$CONTAINERUSER
RUN apt update && apt install -y --no-install-recommends python3-minimal python3-setuptools libcurl3-gnutls && apt clean && rm -rf /var/lib/apt/lists/*
WORKDIR /home/$CONTAINERUSER/pyload
# switchback
USER $CONTAINERUSER
VOLUME /home/$CONTAINERUSER/pyload/config /home/$CONTAINERUSER/pyload/downloads
CMD ["bash", "-c", "pyload --userdir /home/$CONTAINERUSER/pyload/config --storagedir /home/$CONTAINERUSER/pyload/downloads"]
